package com.aiconoa.trainings.java;

public class TicTacToeController {

	private BoardView boardView;
	private TicTacToeRulesEngine rulesEngine;
	private Board board;
	private Player player1;
	private Player player2;
	private Player currentPlayer;

	public TicTacToeController() {
		boardView = new BoardView();
		rulesEngine = new TicTacToeRulesEngine();
	}

	public void run() {
		playANewGame();
	}

	private void playANewGame() {

		initNewGame();

		do {
			playNextTurn();
		} while (!rulesEngine.playerHasWon(currentPlayer, board) && ! board.isFull());

		boardView.displayBoard(board);
		System.out.println("Game Over");

	}

	private void initNewGame() {
		board = new Board(3);
		player1 = new Player('X');
		player2 = new Player('O');
	}

	private void playNextTurn() {
		boardView.displayBoard(board);

		chooseNextCurrentPlayer();

		do {
			Move move = currentPlayer.play(board);

			if (rulesEngine.isValidMove(board, move)) {
				board.placeMove(move);
				break; // exit the loop, the move is valid
			}

			System.out.println("coup invalide, veuillez rejouer");

		} while (true);

	}

	private void chooseNextCurrentPlayer() {
		if(currentPlayer == null // firstTurn 
		|| currentPlayer == player2) {
			currentPlayer = player1;
		} else {
			currentPlayer = player2;
		}
	}

}
