package com.aiconoa.trainings.java;

//-------------
//| X | O | X |
//-------------
//| X | O | X |
//-------------
//| X | O | X |
//-------------

public class BoardView {

	private char printSpace(Space space) {
		if(space.getPlayer() == null) { 
			return ' ';
		} else {
			return space.getPlayer().getItem();
		}		
	}
	
	private void printLine(int y, Board board) {
		for(int x = 1; x <= board.getSize(); x++) {
			System.out.print("|");
			System.out.print(printSpace(board.getSpaceAt(x, y)));
		}
		System.out.println("|");
	}
	
	private void printSpacer() {
		System.out.println("-------");
	}
	
	public void displayBoard(Board board) {
		for(int y = 1 ; y <= board.getSize(); y++) { 
			printSpacer();
			printLine(y, board);
		}
		printSpacer();
	}

}
