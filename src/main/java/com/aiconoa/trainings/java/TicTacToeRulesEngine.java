package com.aiconoa.trainings.java;

public class TicTacToeRulesEngine {

	public boolean isValidMove(Board board, Move move) {
		if (move.getPosition().getX() < 1 || move.getPosition().getX() > board.getSize()) {
			return false;
		}

		if (move.getPosition().getY() < 1 || move.getPosition().getY() > board.getSize()) {
			return false;
		}

		if (board.getSpaceAt(move.getPosition().getX(), move.getPosition().getY()).getPlayer() != null) {
			return false;
		}

		return true;
	}
	
	public boolean playerHasWon(Player player, Board board) {
	
		return playerHasWonOnLines(player, board)
			|| playerHasWonOnColumns(player, board)
			|| playerHasWonOnDiagonalTopLeftBottomRight(player, board)
			|| playerHasWonOnDiagonalBottomLeftTopRight(player, board);
	}

	private boolean playerHasWonOnLines(Player player, Board board) {
		for(int y = 1; y <= board.getSize(); y++) { // go through every line
			if(playerHasWonOnLine(player, board, y)) { return true; }
		}
		return false;
	}
	
	private boolean playerHasWonOnLine(Player player, Board board, int y) {
		for(int x = 1; x <= board.getSize(); x++) { // check that every space of the line is from the current player
			Player playerAtXY = board.getSpaceAt(x, y).getPlayer();
			if(playerAtXY == null || playerAtXY != player) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean playerHasWonOnColumns(Player player, Board board) {
		for(int x = 1; x <= board.getSize(); x++) {
			if(playerHasWonOnColumn(player, board, x)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean playerHasWonOnColumn(Player player, Board board, int x) {
		for(int y = 1; y <= board.getSize(); y++) { // check that every space of the line is from the current player
			Player playerAtXY = board.getSpaceAt(x, y).getPlayer();
			if(playerAtXY == null || playerAtXY != player) {
				return false;
			}
		}
		
		return true;
	}
		
	private boolean playerHasWonOnDiagonalTopLeftBottomRight(Player player, Board board) {
		for(int i = 1; i <= board.getSize(); i++) {
			Player playerAtXY = board.getSpaceAt(i, i).getPlayer();
			if(playerAtXY == null || playerAtXY != player) {
				return false;
			}
		}
		return true;
	}
	
	private boolean playerHasWonOnDiagonalBottomLeftTopRight(Player player, Board board) {
		for(int i = 1; i <= board.getSize(); i++) {
			Player playerAtXY = board.getSpaceAt(i, board.getSize() - i + 1).getPlayer();
			if(playerAtXY == null || playerAtXY != player) {
				return false;
			}
		}
		return true;
	}
	
}
