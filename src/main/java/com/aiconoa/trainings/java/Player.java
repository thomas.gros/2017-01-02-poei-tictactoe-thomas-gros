package com.aiconoa.trainings.java;

import java.util.Scanner;

public class Player {
	private char item;
	
	public Player(char item) {
		this.item = item;
	}
	
	public char getItem() {
		return item;
	}
	
	public Move play(Board board) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Veuillez saisir un coup sous la forme X Y  (un espace entre X et Y)");
		String[] coordinates = scanner.nextLine().split(" ");
		// TODO verify that the coordinates are well-formed and are ints
		return new Move(new Position(Integer.parseInt(coordinates[0]),
									 Integer.parseInt(coordinates[1])),
						this);
	}

}
