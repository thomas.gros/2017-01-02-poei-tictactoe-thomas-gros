package com.aiconoa.trainings.java;

public class Move {

	private Position position;
	private Player player;
	
	public Move(Position position, Player player) {
		super();
		this.position = position;
		this.player = player;
	}

	public Position getPosition() {
		return position;
	}
	
	public Player getPlayer() {
		return player;
	}

}
