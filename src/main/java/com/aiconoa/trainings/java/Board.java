package com.aiconoa.trainings.java;

public class Board {

	private int size;
	
	private Space[][] spaces;
	
	public Board(int size) {
		super();
		this.size = size;
		
		spaces = new Space[size][size];
		for (int x = 0; x < spaces.length; x++) {
			for (int y = 0; y < spaces[x].length; y++) {
				spaces[x][y] = new Space(x, y);
			}
		}
	}

	public Space getSpaceAt(int x, int y) {
		return spaces[x -1 ][y - 1];
	}

	public int getSize() {
		return this.size;
	}

	public boolean isFull() {
		for (int x = 0; x < spaces.length; x++) {
			for (int y = 0; y < spaces[x].length; y++) {
				if(spaces[x][y].getPlayer() == null) {
					return false;
				}
			}
		}
		
		return true;
	}

	public void placeMove(Move move) {
		spaces[move.getPosition().getX() - 1]
				[move.getPosition().getY() - 1]
						.setPlayer(move.getPlayer());
	}

}
