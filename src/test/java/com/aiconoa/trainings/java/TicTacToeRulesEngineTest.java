package com.aiconoa.trainings.java;

import static org.junit.Assert.*;

import org.junit.Test;

public class TicTacToeRulesEngineTest {

	@Test
	public void shouldNotDetectVictory() {
		// GIVEN
		// construire un board 'gagnant' pour un joueur sur une ligne
		// horizontale
		Player player1 = new Player('X');
		Player player2 = new Player('O');

		Board board = new Board(3);
		board.placeMove(new Move(new Position(1, 1), player1));
		board.placeMove(new Move(new Position(1, 2), player2));
		board.placeMove(new Move(new Position(2, 1), player1));

		TicTacToeRulesEngine rulesEngine = new TicTacToeRulesEngine();

		// WHEN / THEN
		assertFalse(rulesEngine.playerHasWon(player1, board));
		assertFalse(rulesEngine.playerHasWon(player2, board));
	}

	@Test
	public void shouldDetectVictoryOnHorizontalLine() {
		// GIVEN
		// construire un board 'gagnant' pour un joueur sur une ligne
		// horizontale
		Player player1 = new Player('X');
		Player player2 = new Player('O');

		Board board = new Board(3);
		board.placeMove(new Move(new Position(1, 1), player1));
		board.placeMove(new Move(new Position(1, 2), player2));
		board.placeMove(new Move(new Position(2, 1), player1));
		board.placeMove(new Move(new Position(2, 2), player2));
		board.placeMove(new Move(new Position(3, 1), player1));

		TicTacToeRulesEngine rulesEngine = new TicTacToeRulesEngine();

		// WHEN / THEN
		assertTrue(rulesEngine.playerHasWon(player1, board));
		assertFalse(rulesEngine.playerHasWon(player2, board));
	}


}
